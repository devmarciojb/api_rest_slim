<?php

namespace App\Controllers;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

use App\DAO\MySQL\CpetCourses\UsersDAO;

final class AuthController
{
    public function login(Request $req, Response $res, array $args): Response
    {
        $data = $req->getParsedBody();
        // code 

        $email = $data['email'];
        $pass = $data['password'];

        $usersDAO = new UsersDAO();
        $user = $usersDAO->getUserByEmail($email);

        if(is_null($user))
            return $res->withStatus(401);
            
        if(!password_verify($pass, $user->getPassword()))
            return $res->withStatus(401);
        
        return $res;
    }
}