<?php

namespace App\Controllers;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

use App\DAO\MySQL\CpetCourses\Courses;
use App\Models\MySQL\CpetCourses\CourseModel;

final class CourseController
{

    public function getCourses(Request $req, Response $res, array $args): Response
    {
        $coursesDAO = new Courses();
        $courses = $coursesDAO->getAllCourses();
        $res = $res->withJson($courses);

        return $res;
    }

    public function getCourse(Request $req, Response $res, array $args): Response
    {
        $id = $args['id'];
        $coursesDAO = new Courses();
        $course = $coursesDAO->getCourse($id);
        $res = $res->withJson($course);
        return $res;
    }

    public function insertCourse(Request $req, Response $res, array $args): Response
    {
        $data = $req->getParsedBody();
        $coursesDAO = new Courses();
        $course = new CourseModel();

        $course
            ->setSchool_id($data['school_id'])
            ->setTitle($data['title'])
            ->setPrice($data['price'])
            ->setSummary($data['summary'])
            ->setEnrolls($data['enrolls'])
            ->setAssessments($data['assessments']);

        $coursesDAO->insertCourse($course);

        $res = $res->withJson([
            "message" => "success!"
        ]);
        return $res;
    }

    public function updateCourse(Request $req, Response $res, array $args): Response
    {
        $id = $args['id'];
        $data = $req->getParsedBody();

        $coursesDAO = new Courses();
        $course = new CourseModel();

        $course
            ->setSchool_id($data['school_id'])
            ->setTitle($data['title'])
            ->setPrice($data['price'])
            ->setSummary($data['summary'])
            ->setEnrolls($data['enrolls'])
            ->setAssessments($data['assessments']);

        $coursesDAO->updateCourse($course, $id);

        $res = $res->withJson([
            "message" => "success!"
        ]);
        return $res;
    }

    public function deleteCourse(Request $req, Response $res, array $args): Response
    {   
        $id = $args['id'];
        $coursesDAO = new Courses();
        $coursesDAO->deleteCourse($id);

        $res = $res->withJson([
            "message" => "success!"
        ]);
        return $res;
    }
}
