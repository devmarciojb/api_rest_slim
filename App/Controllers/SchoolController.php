<?php

namespace App\Controllers;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

use App\DAO\MySQL\CpetCourses\Schools;
use App\Models\MySQL\CpetCourses\SchoolModel;

final class SchoolController
{
    public function getSchools(Request $req, Response $res, array $args): Response
    {
        $schoolsDAO = new Schools();
        $schools = $schoolsDAO->getAllSchools();
        $res = $res->withJson($schools);
        return $res;
    }

    public function getSchool(Request $req, Response $res, array $args): Response
    {
        $id = $args['id'];
        $schoolsDAO = new Schools();
        $school = $schoolsDAO->getSchool($id);
        $res = $res->withJson($school);
        return $res;
    }

    public function insertSchool(Request $req, Response $res, array $args): Response
    {
        $data = $req->getParsedBody();

        $schoolsDAO = new Schools();
        $school = new SchoolModel();

        $school
            ->setName($data['name'])
            ->setPhone($data['phone'])
            ->setAddress($data['address']);

        $schoolsDAO->insertSchool($school);

        $res = $res->withJson([
            'message' => 'success!'
        ]);

        return $res;
    }

    public function updateSchool(Request $req, Response $res, array $args): Response
    {
        $id = $args['id'];
        $data = $req->getParsedBody();

        $schoolsDAO = new Schools();
        $school = new SchoolModel();

        $school
            ->setName($data['name'])
            ->setPhone($data['phone'])
            ->setAddress($data['address']);

        $schoolsDAO->updateSchool($school, $id);

        $res = $res->withJson([
            "message" => "success!"
        ]);
        return $res;
    }

    public function deleteSchool(Request $req, Response $res, array $args): Response
    {
        $id = $args['id'];
        $schoolsDAO = new Schools();
        $schoolsDAO->deleteSchool($id);

        $res = $res->withJson([
            "message" => "success!"
        ]);
        return $res;
    }
}
