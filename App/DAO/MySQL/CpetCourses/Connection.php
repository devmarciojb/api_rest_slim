<?php

namespace App\DAO\MySQL\CpetCourses;

abstract class Connection
{
    /**
     * @var \PDO
     */
    protected $pdo;

    public function __construct()
    {
        $host = getenv('CPET_COURSES_MYSQL_HOST');
        $port = getenv('CPET_COURSES_MYSQL_PORT');
        $user = getenv('CPET_COURSES_MYSQL_USER');
        $pass = getenv('CPET_COURSES_MYSQL_PASSWORD');
        $dbname = getenv('CPET_COURSES_MYSQL_DBNAME');

        $dsn = "mysql:host={$host};dbname={$dbname};port={$port}";

        $this->pdo = new \PDO($dsn, $user, $pass);
        $this->pdo->setAttribute(
            \PDO::ATTR_ERRMODE,
            \PDO::ERRMODE_EXCEPTION
        );
        $this->pdo->exec("SET NAMES 'utf8';");
    }
}
