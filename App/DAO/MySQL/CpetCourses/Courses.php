<?php

namespace App\DAO\MySQL\CpetCourses;
use App\Models\MySQL\CpetCourses\CourseModel;

class Courses extends Connection
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAllCourses(): array
    {
        $obj = $this->pdo->prepare("SELECT
                `schools`.`name` as `school`,
                `courses`.`id` as `id`,
                `courses`.`title` as `course`,
                `courses`.`price` as `price`,
                `courses`.`summary` as `summary`,
                `courses`.`enrolls`	 as `enrolls`,
                `courses`.`assessments` as `assessments`
            FROM `courses`
            INNER JOIN `schools` ON `courses`.`school_id` = `schools`.`id`
            WHERE
                `courses`.`deleted` = 0
            ORDER BY `courses`.`id`
        ");

        $obj->execute();
        $courses = $obj->fetchAll(\PDO::FETCH_ASSOC);
        return $courses;
    }

    public function getCourse(int $id): array
    {
        $obj = $this->pdo->prepare("SELECT
                `schools`.`name` as `school`,
                `courses`.`id` as `id`,
                `courses`.`title` as `course`,
                `courses`.`price` as `price`,
                `courses`.`summary` as `summary`,
                `courses`.`enrolls`	 as `enrolls`,
                `courses`.`assessments` as `assessments`
            FROM `courses`
            INNER JOIN `schools` ON `courses`.`school_id` = `schools`.`id`
            WHERE `courses`.`id` = :id
        ");
        $obj->bindValue(':id', $id);
        $obj->execute();
        $course = $obj->fetchAll(\PDO::FETCH_ASSOC);
        return $course;
    }

    public function insertCourse(CourseModel $course): void
    {
        $obj = $this->pdo
            ->prepare("INSERT INTO
                `courses`
                (`school_id`, `title`, `price`, `summary`, `enrolls`, `assessments`) VALUES (?, ?, ?, ?, ?, ?);");

        $obj->execute([
            $course->getSchool_id(),
            $course->getTitle(),
            $course->getPrice(),
            $course->getSummary(),
            $course->getEnrolls(),
            $course->getAssessments()
        ]);
    }

    public function updateCourse(CourseModel $course, int $id): void
    {
        $obj = $this->pdo
            ->prepare("UPDATE
                `courses` SET
                    `school_id` = ?,
                    `title` = ?,
                    `price` = ?,
                    `summary` = ?,
                    `enrolls` = ?,
                    `assessments` = ?
                WHERE `courses`.`id` = $id
            ");
        $obj->execute([
            $course->getSchool_id(),
            $course->getTitle(),
            $course->getPrice(),
            $course->getSummary(),
            $course->getEnrolls(),
            $course->getAssessments()
        ]);
    }

    public function deleteCourse(int $id)
    {
        $obj = $this->pdo
            ->prepare("UPDATE
                `courses` SET
                `deleted` = true
                WHERE `courses`.`id` = $id
            ");
        $obj->execute();
    }
}