<?php

namespace App\DAO\MySQL\CpetCourses;
use App\Models\MySQL\CpetCourses\SchoolModel;

class Schools extends Connection
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAllSchools(): array
    {
        $obj = $this->pdo->prepare("SELECT
            * FROM `schools` 
            WHERE `schools`.`deleted` = 0
        ");

        $obj->execute();
        $schools = $obj->fetchAll(\PDO::FETCH_ASSOC);

        return $schools;
    }

    public function getSchool(int $id): array
    {
        $obj = $this->pdo->prepare("SELECT
            * FROM `schools`
            WHERE `schools`.`id` = :id
        ");
        $obj->bindValue(':id', $id);
        $obj->execute();
        $school = $obj->fetchAll(\PDO::FETCH_ASSOC);

        return $school;
    }

    public function insertSchool(SchoolModel $school): void
    {
        $obj = $this->pdo
            ->prepare("INSERT INTO
                `schools` VALUES(
                    null,
                    :name,
                    :phone,
                    :address
                );
            ");

        $obj->execute([
            'name' => $school->getName(),
            'phone' => $school->getPhone(),
            'address' => $school->getAddress()
        ]);
    }

    public function updateSchool(SchoolModel $school, int $id): void
    {
        $obj = $this->pdo
            ->prepare("UPDATE
                `schools` SET
                    `name` = ?,
                    `phone` = ?,
                    `address` = ?
                WHERE `schools`.`id` = $id
            ");

        $obj->execute([
            $school->getName(),
            $school->getPhone(),
            $school->getAddress()
        ]);
    }

    public function deleteSchool(int $id): void
    {
        $obj = $this->pdo
            ->prepare("UPDATE
                `schools` SET
                `deleted` = true
                WHERE `schools`.`id` = $id
            ");
        $obj->execute();
    }
}