<?php

namespace App\DAO\MySQL\CpetCourses;
use App\Models\MySQL\CpetCourses\UserModel;

class UsersDAO extends Connection
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getUserByEmail(string $email): ?UserModel
    {
        $obj = $this->pdo
            ->prepare("SELECT
                    id,
                    email,
                    name,
                    password
                FROM `users`
                WHERE email = :email
            ");
        $obj->bindParam(':email', $email);
        $obj->execute();
        $users = $obj->fetchAll(\PDO::FETCH_ASSOC);
        if(count($users) === 0)
            return null;
        $user = new UserModel();
        $user->setId($users[0]['id'])
        ->setEmail($users[0]['email'])
        ->setName($users[0]['name'])
        ->setPassword($users[0]['password']);

        return $user;
    }
}