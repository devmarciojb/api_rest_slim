<?php

namespace App\Models\MySQL\CpetCourses;

final class CourseModel
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $school_id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var float
     */
    private $price;

    /**
     * @var string
     */
    private $summary;

    /**
     * @var int
     */
    private $enrolls;

    /**
     * @var int
     */
    private $assessments;

    

    /**
     *
     * @return  int
     */ 
    public function getId(): int
    {
        return $this->id;
    }

    /**
     *
     * @return  int
     */ 
    public function getSchool_id(): int
    {
        return $this->school_id;
    }

    /**
     *
     * @param  int  $school_id
     *
     * @return  self
     */ 
    public function setSchool_id(int $school_id): CourseModel
    {
        $this->school_id = $school_id;

        return $this;
    }

    /**
     *
     * @return  string
     */ 
    public function getTitle(): string
    {
        $title = utf8_encode($this->title);
        
        return $title;
    }

    /**
     *
     * @param  string  $title
     *
     * @return  self
     */ 
    public function setTitle(string $title): CourseModel
    {
        $this->title = $title;

        return $this;
        
    }

    /**
     * Get the value of price
     *
     * @return  float
     */ 
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     *
     * @param  float  $price
     *
     * @return  self
     */ 
    public function setPrice(float $price): CourseModel
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get the value of summary
     *
     * @return  string
     */ 
    public function getSummary(): string
    {
        $summary = utf8_encode($this->summary);

        return $summary;
    }

    /**
     * Set the value of summary
     *
     * @param  string  $summary
     *
     * @return  self
     */ 
    public function setSummary(string $summary): CourseModel
    {
        $this->summary = $summary;

        return $this;
    }

    /**
     * Get the value of enrolls
     *
     * @return  int
     */ 
    public function getEnrolls(): int
    {
        return $this->enrolls;
    }

    /**
     *
     * @param  int  $enrolls
     *
     * @return  self
     */ 
    public function setEnrolls(int $enrolls): CourseModel
    {
        $this->enrolls = $enrolls;

        return $this;
    }

    /**
     *
     * @return  int
     */ 
    public function getAssessments(): int
    {
        return $this->assessments;
    }

    /**
     *
     * @param  int  $assessments
     *
     * @return  self
     */ 
    public function setAssessments(int $assessments): CourseModel
    {
        $this->assessments = $assessments;

        return $this;
    }
}