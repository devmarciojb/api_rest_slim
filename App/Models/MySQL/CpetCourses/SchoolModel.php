<?php

namespace App\Models\MySQL\CpetCourses;

final class SchoolModel
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $phone;
    /**
     * @var string
     */
    private $address;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        $name = utf8_encode($this->name);
        return $name;
    }

    /**
     * @param string
     * @return SchoolModel
     */
    public function setName(string $name): SchoolModel
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        $phone = utf8_encode($this->phone);
        return $phone;
    }

    /**
     * @param string
     * @return SchoolModel
     */
    public function setPhone(string $phone): SchoolModel
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        $address = utf8_encode($this->address);
        return $address;
    }

    /**
     * @param string
     * @return SchoolModel
     */
    public function setAddress(string $address): SchoolModel
    {
        $this->address = $address;
        return $this;
    }

}
