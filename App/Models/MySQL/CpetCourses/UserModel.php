<?php

namespace App\Models\MySQL\CpetCourses;

final class UserModel
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $password;
    

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return self
     */ 
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */ 
    public function getName(): string
    {
        $name = utf8_encode($this->name);
        return $name;
    }

    /**
     * @param string $name
     * @return self
     */ 
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */ 
    public function getEmail(): string
    {
        $email = utf8_encode($this->email);
        return $email;
    }

    /**
     *
     * @param string $email
     * @return self
     */ 
    public function setEmail(string $email): self
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */ 
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return self
     */ 
    public function setPassword(string $password): self
    {
        $this->password = $password;
        return $this;
    }
}
