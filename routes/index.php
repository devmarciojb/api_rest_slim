<?php

use function src\slimConfiguration;
use App\Controllers\{
    CourseController,
    SchoolController,
    AuthController
};

$app = new \Slim\App(slimConfiguration());

// --------------------------------------- 

$app->post('/login', AuthController::class . ':login');

$app->group('', function() use ($app) {
    $app->get('/schools', SchoolController::class . ':getSchools');
    $app->get('/school/{id}', SchoolController::class . ':getSchool');
    $app->post('/school', SchoolController::class . ':insertSchool');
    $app->put('/school/{id}', SchoolController::class . ':updateSchool');
    $app->delete('/school/{id}', SchoolController::class . ':deleteSchool');

    $app->get('/courses', CourseController::class . ':getCourses');
    $app->get('/course/{id}', CourseController::class . ':getCourse');
    $app->post('/course', CourseController::class . ':insertCourse');
    $app->put('/course/{id}', CourseController::class . ':updateCourse');
    $app->delete('/course/{id}', CourseController::class . ':deleteCourse');
});

// ---------------------------------------

$app->run();